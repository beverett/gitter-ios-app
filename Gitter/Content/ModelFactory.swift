import Foundation
import CoreData

class ModelFactory<Model: NSManagedObject> {

    let entityName: String
    let context: NSManagedObjectContext

    init(entityName: String, context: NSManagedObjectContext) {
        self.entityName = entityName
        self.context = context
    }

    func getOrCreate(byId id: String) throws -> Model {
        return try get(byId: id) ?? create()
    }

    func getOrCreateSingleton() throws -> Model {
        return try getSingleton() ?? create()
    }

    func get(byId id: String) throws -> Model? {
        let fetchRequest = NSFetchRequest<Model>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        fetchRequest.fetchLimit = 1

        return try context.fetch(fetchRequest).first
    }

    func get(byIds ids: [String]) throws -> [Model] {
        let fetchRequest = NSFetchRequest<Model>()
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "id IN %@", ids)
        return try context.fetch(fetchRequest)
    }

    func create() -> Model {
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        return NSManagedObject(entity: entity!, insertInto: context) as! Model
    }

    func getMapped(byIds ids: [String]) throws -> [String : Model] {
        let models = try get(byIds: ids)

        return models.reduce([String : Model](), { (hash, object) -> [String : Model] in
            var mutableHash = hash
            mutableHash[(object as NSManagedObject).value(forKey: "id") as! String] = object
            return mutableHash
        })
    }

    private func getSingleton() throws -> Model? {
        let fetchRequest = NSFetchRequest<Model>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        fetchRequest.fetchLimit = 1

        return try context.fetch(fetchRequest).first
    }
}
