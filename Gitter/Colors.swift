import UIKit

class Colors {

    static func caribbeanColor() -> UIColor {
        return hexToColor(0x46BC99)
    }

    static func jaffaColor() -> UIColor {
        return hexToColor(0xf68d42)
    }

    static func rubyColor() -> UIColor {
        return hexToColor(0xed1965)
    }

    static func troupeGreenColor() -> UIColor {
        return hexToColor(0x1dce73)
    }

    static func placeholderAvatarColor() -> UIColor {
        return hexToColor(0xeeeeee)
    }

    private static func hexToColor(_ hex:Int) -> UIColor {
        let red = (hex >> 16) & 0xff
        let green = (hex >> 8) & 0xff
        let blue = hex & 0xff

        return UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0)
    }
}
