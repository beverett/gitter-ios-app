import Foundation
import CoreData

class AdminGroupCollection: NSManagedObject {
    @NSManaged var groups: Set<Group>
}
