import Foundation
import CoreData

class UserRoomCollection: NSManagedObject {
    @NSManaged var rooms: Set<Room>
}
