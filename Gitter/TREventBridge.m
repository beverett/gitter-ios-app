//
//  TREventBridge.m
//  Troupe
//
//  Created by Andrew Newdigate on 06/02/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import "TREventBridge.h"
#import "TREventController.h"
#import "TRTroupeClient.h"
#import "TRAuthController.h"
#import "TRBackingOffRetrier.h"

static TREventBridge *sharedSingleton;

@interface TREventBridge ()

@property (strong, nonatomic) TREventController *eventController;

@property (nonatomic, strong) TRBackingOffRetrier *backingOff;

- (void) loginIfReady;

- (void) logout;

@end

@implementation TREventBridge

+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedSingleton = [[TREventBridge alloc] init];
        
    }
}

+ (TREventBridge *) sharedInstance
{
    return sharedSingleton;
}

- (id)init
{
    self = [super init];
    if (self) {        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];

        [center addObserver:self
                   selector:@selector(troupeUrlChanged:)
                       name:@"TroupeUrlChanged"
                     object:nil];
        
        [center addObserver:self
                   selector:@selector(troupeSignedOut:)
                       name:@"TroupeSignedOut"
                     object:nil];
        
        [center addObserver:self
                   selector:@selector(troupeAuthenticationReady:)
                       name:@"TroupeAuthenticationReady"
                     object:nil];

        
        [self loginIfReady];
        
    }
    return self;
}

- (void)loginIfReady {
    [self logout];

    if(![[TRAuthController sharedInstance] authenticated]) {
        return;
    }
    
    qldebug(@"Event bridge is attempting to login");
    
    if(self.backingOff != nil) [self.backingOff cancel];
    
    self.backingOff = [[TRBackingOffRetrier alloc] init];
    
    __block TREventBridge *this = self;
    
    [self.backingOff retryUntilSuccessful:^() {
        /* If not authenticated, theres no point in retrying */
        if(![[TRAuthController sharedInstance] authenticated]) {
            [this.backingOff successCallback];
        }
        
        TRTroupeClient *restClient = [[TRTroupeClient alloc] init];
        
        [restClient getCurrentUser:^(NSDictionary *user) {
            NSString *userId = [user objectForKey:@"id"];
            
            qldebug(@"Event bridge has userId. Connecting to Faye");
            
            this.eventController = [[TREventController alloc] initWithUserId: userId delegate:self];
            
            [this.backingOff successCallback];
        } failure:^(NSError *error) {
            qlerror(@"%@", error);
            
            [this.backingOff failureCallback];
        }];

    }];

}


- (BOOL)subscriptionIsReady {
    if(self.eventController == nil) {
        return NO;
    }
    
    return self.eventController.subscriptionReady;
}


- (void)logout {
    if(self.eventController == nil) {
        return;
    }
    
    [self.eventController disconnect];
    self.eventController = nil;
}

- (void) troupeUrlChanged:(NSNotification *)notification {
    [self logout];
    [self loginIfReady];
}

- (void) troupeSignedOut:(NSNotification *)notification {
    [self logout];
}

- (void) troupeAuthenticationReady:(NSNotification *)notification {
    [self loginIfReady];
}

- (void) applicationDidEnterBackground {
    // Disable until we figure out why this is crashing
    [self.eventController disconnect];
}

- (void) applicationWillTerminate {
    [self.eventController disconnect];
}

- (void) applicationWillEnterForeground {
    // Disable until we figure out why this is crashing
    [self.eventController reconnect];
}


@end
